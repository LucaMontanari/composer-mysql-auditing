from airflow.contrib.operators.gcp_sql_operator import CloudSqlQueryOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from six.moves.urllib.parse import quote_plus
from airflow import models

import logging
import os

# Environment variables - CloudSQL
GCP_PROJECT_ID = models.Variable.get('PROJECT_ID')
GCP_SQL_REGION = models.Variable.get('GCP_SQL_REGION')
GCSQL_MYSQL_INSTANCE_NAME_QUERY = models.Variable.get('GCSQL_MYSQL_INSTANCE_NAME_QUERY')
GCSQL_MYSQL_DATABASE_NAME = models.Variable.get('GCSQL_MYSQL_DATABASE_NAME')
GCSQL_MYSQL_USER = models.Variable.get('GCSQL_MYSQL_USER')
GCSQL_MYSQL_PASSWORD = models.Variable.get('GCSQL_MYSQL_PASSWORD')
GCSQL_MYSQL_PUBLIC_IP = models.Variable.get('GCSQL_MYSQL_PUBLIC_IP')
GCSQL_MYSQL_PUBLIC_PORT = models.Variable.get('GCSQL_MYSQL_PUBLIC_PORT')
GCSQL_MYSQL_AUDIT_TABLE = models.Variable.get('GCSQL_MYSQL_AUDIT_TABLE')

########################## MYSQL CONNECTION ########################################

mysql_kwargs = dict(
    user=quote_plus(GCSQL_MYSQL_USER),
    password=quote_plus(GCSQL_MYSQL_PASSWORD),
    public_port=GCSQL_MYSQL_PUBLIC_PORT,
    public_ip=quote_plus(GCSQL_MYSQL_PUBLIC_IP),
    project_id=quote_plus(GCP_PROJECT_ID),
    location=quote_plus(GCP_SQL_REGION),
    instance=quote_plus(GCSQL_MYSQL_INSTANCE_NAME_QUERY),
    database=quote_plus(GCSQL_MYSQL_DATABASE_NAME)
)        

# MySQL: connect directly via TCP (non-SSL)
os.environ['AIRFLOW_CONN_PUBLIC_MYSQL_TCP'] = \
    "gcpcloudsql://{user}:{password}@{public_ip}:{public_port}/{database}?" \
    "database_type=mysql&" \
    "project_id={project_id}&" \
    "location={location}&" \
    "instance={instance}&" \
    "use_proxy=False&" \
    "use_ssl=False".format(**mysql_kwargs)

####################################################################################

def quote(string):
    return '"' + string + '"'

def build_audit_pre_step(step_name, trigger = "all_success"):

    dag_name = "{{ dag.dag_id }}"
    record_id = dag_name + "-{{ dag_run.execution_date }}-" + step_name
    ML_MODEL_ID = "{{ dag_run.conf['ML_MODEL_ID'] }}"
    MODEL_ID = "{{ dag_run.conf['MODEL_ID'] }}"

    pre_step_insert_query = ("INSERT INTO " + GCSQL_MYSQL_AUDIT_TABLE + " (id, dag_name, ml_model_id, model_id, step_name, creation_time, state, last_update) "
                             "VALUES (" + quote(record_id) + ',' + quote(dag_name) + ','
                                        + quote(ML_MODEL_ID) + ',' 
                                        + quote(MODEL_ID) + ',' + quote(step_name) + ',' + "CURRENT_TIMESTAMP" + ','
                                        + quote("running") + ',' + "CURRENT_TIMESTAMP" +
                             ");")
    logging.info(pre_step_insert_query)

    return CloudSqlQueryOperator(
        gcp_cloudsql_conn_id = "public_mysql_tcp",
        task_id = "audit_pre_" + step_name,
        sql = pre_step_insert_query,
        trigger = trigger
    )

def build_audit_post_step(step_name, update_key):

    dag_name = "{{ dag.dag_id }}"
    record_id = dag_name + "-{{ dag_run.execution_date }}-" + step_name
    update_output  = ""

    if update_key is not None:
        update_output = ", output = '{{ task_instance.xcom_pull(key = '" + update_key + "') }}' "

    post_step_update_query = ("UPDATE " + GCSQL_MYSQL_AUDIT_TABLE + " " +
                              "SET state = \"done\", last_update = CURRENT_TIMESTAMP " + update_output +
                              "WHERE id = " + quote(record_id) + 
                              ";")
    logging.info(post_step_update_query)

    fail_step_update_query = ("UPDATE " + GCSQL_MYSQL_AUDIT_TABLE + " " +
                              "SET state = \"fail\", last_update = CURRENT_TIMESTAMP " +
                              "WHERE id = " + quote(record_id) + 
                              ";")
    logging.info(fail_step_update_query)

    audit_post_step = CloudSqlQueryOperator(
        gcp_cloudsql_conn_id = "public_mysql_tcp",
        task_id = "audit_post_" + step_name,
        sql = post_step_update_query
    )

    audit_fail_step = CloudSqlQueryOperator(
        gcp_cloudsql_conn_id = "public_mysql_tcp",
        task_id = "audit_fail_" + step_name,
        sql = fail_step_update_query,
        trigger_rule='one_failed'
    )

    def raiseValueError(s):
        raise ValueError(s)

    audit_fail_raise = PythonOperator(
        task_id = "audit_fail_raise_" + step_name,
        python_callable = lambda x: raiseValueError(step_name + ": failed")
    )

    dummy_post_step = DummyOperator(
        task_id = "dummy_post_" + step_name,
        trigger_rule='none_failed'
    )

    audit_fail_step >> audit_fail_raise
    [audit_post_step, audit_fail_raise] >> dummy_post_step
    return [audit_post_step, audit_fail_step], dummy_post_step

def audit_and_execute(step, trigger = "all_success", update_key = None):
    pre = build_audit_pre_step(step.task_id, trigger)
    post_in, post_out = build_audit_post_step(step.task_id, update_key)
    pre >> step >> post_in
    return pre, post_out