# Composer CloudSQL Auditing Library
Questa libreria ha lo scopo di tracciare l'avanzamento di un DAG Airflow su Composer tramite un sistema di auditing su Google CloudSQL. 

## Requirements:
- mysql-connector-python==8.0.17

## Prerequisiti:
- Composer up and runing e con le seguenti variabili globali configurate:  
	- PROJECT_ID  
	(id progetto gcp)
	- GCP_SQL_REGION  
	(regione del server mysql)
	- SQL_MYSQL_INSTANCE_NAME_QUERY  
	(nome dell'istanza mysql)
	- SQL_MYSQL_DATABASE_NAME  
	(nome del db contenente la tabella di audit)
	- SQL_MYSQL_AUDIT_TABLE  
	(nome della tabella di audit)
	- SQL_MYSQL_USER
	- SQL_MYSQL_PASSWORD
	- SQL_MYSQL_PUBLIC_IP
	- SQL_MYSQL_PUBLIC_PORT  

Per lo schema della tabella di audit fare riferimento al seguente documento (slide 10):  
https://docs.google.com/presentation/d/1-sqPCauvzZLjKt1OZydod0IjxJ1qxB1usUuJkSM7-wo/edit#slide=id.g5e58255453_0_191