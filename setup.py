import setuptools

setuptools.setup(
    name="composer-mysql-auditing",
    version="0.0.2",
    author="Emme",
    author_email="luca.montanari@injenia.it",
    description="Package for auditing composer execution progress on Google CloudSQL",
    packages=setuptools.find_packages(),
    install_requires=[
       'mysql-connector-python==8.0.17'
    ]
)